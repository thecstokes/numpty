﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Numpty
{
    /// <summary>
    /// Interaction logic for SectionListItem.xaml
    /// </summary>
    /// 

    public class SectionListItemData
    {
        public string type;
        public int sectionNumber;
        public string building;
        public string room;
        public string instructor;

    }

    public partial class SectionListItem : UserControl
    {

        public SectionListItemData Data
        {
            get
            {
                return (SectionListItemData)GetValue(DataProperty);
            }
            set
            {
                SetValue(DataProperty, value);
            }
        }

        public static readonly DependencyProperty DataProperty = DependencyProperty.Register("Data", typeof(SectionListItemData), 
            typeof(SectionListItem));

        public SectionListItem()
        {
            InitializeComponent();
            this.DataContext = this;
            this.Data.type = "Core";
        }
    }
}
