﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numpty.ViewModels
{
    public class TestClass
    {
        public string Name { get; set; }
    }


    public class TabsViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<TestClass> Tester { get; set; } = new ObservableCollection<TestClass>()
        {
            new TestClass() {Name = "Alpha"},
            new TestClass() {Name = "Beta"},
            new TestClass() {Name = "Charlie"}
        };

    }
}
