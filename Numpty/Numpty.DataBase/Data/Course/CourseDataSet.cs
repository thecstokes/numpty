﻿using Numpty.DataBase.Data.Core;
using Numpty.DataBase.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numpty.DataBase.Data
{
    public partial class CourseDataSet : BaseDataSet<CourseDataSet, Course>
    {
        #region Private Constructor(s).
        private CourseDataSet(Course course) : base(course)
        {

        }
        #endregion

        #region MapEnum Implementation.
        protected override CourseDataSet Instance => this;
        #endregion
    }
}
