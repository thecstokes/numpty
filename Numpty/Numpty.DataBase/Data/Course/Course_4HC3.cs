﻿using Numpty.DataBase.Data.Core;
using Numpty.DataBase.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numpty.DataBase.Data
{
    public partial class CourseDataSet : BaseDataSet<CourseDataSet, Course>
    {
        public static readonly CourseDataSet _4HC3 = new CourseDataSet(new Course
        {
            ID = DBUtils.NextGUID(),
            Name = "Human Computer Interfaces",
            Code = "4HC3",
            Department = DepartmentDataSet.SoftwareEngineering.Data,
            Schedule = new TimeSchedule
            {
                ID = DBUtils.NextGUID(),
                Slots = new List<TimeSlot>
                {
                    new TimeSlot
                    {
                        ID = DBUtils.NextGUID(),
                        Type = TimeSlotType.Core,
                        Day = WeekDay.Monday,
                        Start = Period.Five,
                        End = Period.Six
                    },
                    new TimeSlot
                    {
                        ID = DBUtils.NextGUID(),
                        Type = TimeSlotType.Core,
                        Day = WeekDay.Tuesday,
                        Start = Period.Six,
                        End = Period.Seven
                    },
                    new TimeSlot
                    {
                        ID = DBUtils.NextGUID(),
                        Type = TimeSlotType.Core,
                        Day = WeekDay.Thursday,
                        Start = Period.Five,
                        End = Period.Six
                    }
                }
            }
        });
    }
}
