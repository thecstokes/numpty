﻿using Numpty.DataBase.Data.Core;
using Numpty.DataBase.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numpty.DataBase.Data
{
    public partial class CourseDataSet : BaseDataSet<CourseDataSet, Course>
    {
        public static readonly CourseDataSet _1G03 = new CourseDataSet(new Course
        {
            ID = DBUtils.NextGUID(),
            Name = "Rocks",
            Code = "1G03",
            Department = DepartmentDataSet.EnvironmentalScience.Data,
            Schedule = new TimeSchedule
            {
                ID = DBUtils.NextGUID(),
                Slots = new List<TimeSlot>
                {
                    #region Core(s).
                    new TimeSlot
                    {
                        ID = DBUtils.NextGUID(),
                        Type = TimeSlotType.Core,
                        Day = WeekDay.Tuesday,
                        Start = Period.Two,
                        End = Period.Three
                    },
                    new TimeSlot
                    {
                        ID = DBUtils.NextGUID(),
                        Type = TimeSlotType.Core,
                        Day = WeekDay.Wednesday,
                        Start = Period.Three,
                        End = Period.Four
                    },
                    new TimeSlot
                    {
                        ID = DBUtils.NextGUID(),
                        Type = TimeSlotType.Core,
                        Day = WeekDay.Friday,
                        Start = Period.Two,
                        End = Period.Three
                    },
	                #endregion

                    #region Lab(s).
                    new TimeSlot
                    {
                        ID = DBUtils.NextGUID(),
                        Type = TimeSlotType.Lab,
                        Day = WeekDay.Monday,
                        Start = Period.Three,
                        End = Period.Four
                    },
                    new TimeSlot
                    {
                        ID = DBUtils.NextGUID(),
                        Type = TimeSlotType.Lab,
                        Day = WeekDay.Tuesday,
                        Start = Period.Three,
                        End = Period.Four
                    },
                    new TimeSlot
                    {
                        ID = DBUtils.NextGUID(),
                        Type = TimeSlotType.Lab,
                        Day = WeekDay.Wednesday,
                        Start = Period.Three,
                        End = Period.Four
                    },
                    new TimeSlot
                    {
                        ID = DBUtils.NextGUID(),
                        Type = TimeSlotType.Lab,
                        Day = WeekDay.Thursday,
                        Start = Period.Three,
                        End = Period.Four
                    },
                    new TimeSlot
                    {
                        ID = DBUtils.NextGUID(),
                        Type = TimeSlotType.Lab,
                        Day = WeekDay.Friday,
                        Start = Period.Three,
                        End = Period.Four
                    } 
	                #endregion
                }
            }
        });
    }
}
