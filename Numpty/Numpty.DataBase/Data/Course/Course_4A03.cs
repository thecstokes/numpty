﻿using Numpty.DataBase.Data.Core;
using Numpty.DataBase.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numpty.DataBase.Data
{
    public partial class CourseDataSet : BaseDataSet<CourseDataSet, Course>
    {
        public static readonly CourseDataSet _4A03 = new CourseDataSet(new Course
        {
            ID = DBUtils.NextGUID(),
            Name = "Ethics and Sustainability",
            Code = "4A03",
            Department = DepartmentDataSet.Engineering.Data,
            Schedule = new TimeSchedule
            {
                ID = DBUtils.NextGUID(),
                Slots = new List<TimeSlot>
                {
                    new TimeSlot
                    {
                        ID = DBUtils.NextGUID(),
                        Type = TimeSlotType.Core,
                        Day = WeekDay.Monday,
                        Start = Period.Twelve,
                        End = Period.Fourteen
                    }
                }
            }
        });
    }
}
