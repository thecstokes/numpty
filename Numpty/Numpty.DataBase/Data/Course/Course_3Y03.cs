﻿using Numpty.DataBase.Data.Core;
using Numpty.DataBase.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numpty.DataBase.Data
{
    public partial class CourseDataSet : BaseDataSet<CourseDataSet, Course>
    {
        public static readonly CourseDataSet _3Y03 = new CourseDataSet(new Course
        {
            ID = DBUtils.NextGUID(),
            Name = "Engineering Statistics",
            Code = "3Y03",
            Department = DepartmentDataSet.Math.Data,
            Schedule = new TimeSchedule
            {
                ID = DBUtils.NextGUID(),
                Slots = new List<TimeSlot>
                {
                    new TimeSlot
                    {
                        ID = DBUtils.NextGUID(),
                        Type = TimeSlotType.Core,
                        Day = WeekDay.Monday,
                        Start = Period.Two,
                        End = Period.Three
                    },
                    new TimeSlot
                    {
                        ID = DBUtils.NextGUID(),
                        Type = TimeSlotType.Core,
                        Day = WeekDay.Tuesday,
                        Start = Period.Three,
                        End = Period.Four
                    },
                    new TimeSlot
                    {
                        ID = DBUtils.NextGUID(),
                        Type = TimeSlotType.Core,
                        Day = WeekDay.Thursday,
                        Start = Period.Two,
                        End = Period.Three
                    }
                }
            }
        });
    }
}
