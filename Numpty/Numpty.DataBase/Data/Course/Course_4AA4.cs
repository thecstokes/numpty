﻿using Numpty.DataBase.Data.Core;
using Numpty.DataBase.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numpty.DataBase.Data
{
    public partial class CourseDataSet : BaseDataSet<CourseDataSet, Course>
    {
        public static readonly CourseDataSet _4AA4 = new CourseDataSet(new Course
        {
            ID = DBUtils.NextGUID(),
            Name = "Real Time Systems",
            Code = "4AA4",
            Department = DepartmentDataSet.SoftwareEngineering.Data,
            Schedule = new TimeSchedule
            {
                ID = DBUtils.NextGUID(),
                Slots = new List<TimeSlot>
                    {
                        #region Core(s).
                        new TimeSlot
                        {
                            ID = DBUtils.NextGUID(),
                            Type = TimeSlotType.Core,
                            Day = WeekDay.Monday,
                            Start = Period.One,
                            End = Period.Two
                        },
                        new TimeSlot
                        {
                            ID = DBUtils.NextGUID(),
                            Type = TimeSlotType.Core,
                            Day = WeekDay.Tuesday,
                            Start = Period.One,
                            End = Period.Two
                        },
                        new TimeSlot
                        {
                            ID = DBUtils.NextGUID(),
                            Type = TimeSlotType.Core,
                            Day = WeekDay.Thursday,
                            Start = Period.One,
                            End = Period.Two
                        },
                        #endregion
                        #region Lab(s).
                        new TimeSlot
                        {
                            ID = DBUtils.NextGUID(),
                            Type = TimeSlotType.Lab,
                            Day = WeekDay.Monday,
                            Start = Period.Five,
                            End = Period.Eight
                        },
                        new TimeSlot
                        {
                            ID = DBUtils.NextGUID(),
                            Type = TimeSlotType.Lab,
                            Day = WeekDay.Tuesday,
                            Start = Period.Five,
                            End = Period.Eight
                        },
                        new TimeSlot
                        {
                            ID = DBUtils.NextGUID(),
                            Type = TimeSlotType.Lab,
                            Day = WeekDay.Wednesday,
                            Start = Period.Five,
                            End = Period.Eight
                        },
                        new TimeSlot
                        {
                            ID = DBUtils.NextGUID(),
                            Type = TimeSlotType.Lab,
                            Day = WeekDay.Thursday,
                            Start = Period.Five,
                            End = Period.Eight
                        },
                        new TimeSlot
                        {
                            ID = DBUtils.NextGUID(),
                            Type = TimeSlotType.Lab,
                            Day = WeekDay.Friday,
                            Start = Period.Five,
                            End = Period.Eight
                        } 
	                    #endregion
                    }
            }
        });
    }
}
