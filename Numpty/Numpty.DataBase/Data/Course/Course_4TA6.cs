﻿using Numpty.DataBase.Data.Core;
using Numpty.DataBase.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numpty.DataBase.Data
{
    public partial class CourseDataSet : BaseDataSet<CourseDataSet, Course>
    {
        public static readonly CourseDataSet _4TA6 = new CourseDataSet(new Course
        {
            ID = DBUtils.NextGUID(),
            Name = "Capstone",
            Code = "4TB6",
            Department = DepartmentDataSet.SoftwareEngineering.Data,
            Schedule = new TimeSchedule
            {
                ID = DBUtils.NextGUID(),
                Slots = new List<TimeSlot>
                {
                    new TimeSlot
                    {
                        ID = DBUtils.NextGUID(),
                        Type = TimeSlotType.Core,
                        Day = WeekDay.Tuesday,
                        Start = Period.Eight,
                        End = Period.Nine
                    },
                    new TimeSlot
                    {
                        ID = DBUtils.NextGUID(),
                        Type = TimeSlotType.Core,
                        Day = WeekDay.Wednesday,
                        Start = Period.Eight,
                        End = Period.Nine
                    },
                    new TimeSlot
                    {
                        ID = DBUtils.NextGUID(),
                        Type = TimeSlotType.Core,
                        Day = WeekDay.Thursday,
                        Start = Period.Eight,
                        End = Period.Nine
                    }
                }
            }
        });
    }
}
