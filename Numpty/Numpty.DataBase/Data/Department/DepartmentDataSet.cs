﻿using Numpty.DataBase.Data.Core;
using Numpty.DataBase.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numpty.DataBase.Data
{
    public partial class DepartmentDataSet : BaseDataSet<DepartmentDataSet, Department>
    {
        public static readonly DepartmentDataSet SoftwareEngineering = new DepartmentDataSet(new Department
        {
            ID = DBUtils.NextGUID(),
            Name = "Software Engineering"
        });

        public static readonly DepartmentDataSet Engineering = new DepartmentDataSet(new Department
        {
            ID = DBUtils.NextGUID(),
            Name = "Engineering"
        });

        public static readonly DepartmentDataSet EnvironmentalScience = new DepartmentDataSet(new Department
        {
            ID = DBUtils.NextGUID(),
            Name = "Environmental Science"
        });

        public static readonly DepartmentDataSet Math = new DepartmentDataSet(new Department
        {
            ID = DBUtils.NextGUID(),
            Name = "Math"
        });

        public DepartmentDataSet(Department department) : base(department)
        {

        }

        protected override DepartmentDataSet Instance => this;
    }
}
