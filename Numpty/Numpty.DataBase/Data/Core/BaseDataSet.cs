﻿using Numpty.DataBase.DataModels.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numpty.DataBase.Data.Core
{
    public abstract class BaseDataSet<TElement, TData> : MapEnum<TElement>
        where TElement : MapEnum<TElement>
        where TData : IDataModel
    {
        public BaseDataSet(TData data)
        {
            Data = data;
        }

        public TData Data { get; set; }

        //public abstract List<TData> DataSet { get; }

        //private static int _id;

        //public static int NextGUID()
        //{
        //    return (++_id);
        //}

        //protected override BaseDataSet<TData> Instance => this;
    }
}
