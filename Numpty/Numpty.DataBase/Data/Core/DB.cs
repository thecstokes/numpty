﻿using Numpty.DataBase.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numpty.DataBase.Data.Core
{
    public static class DB
    {
        public static List<Course> Courses { get; } = CourseDataSet.Values.Select(c => c.Data).ToList();

        public static List<Department> Departments { get; } = DepartmentDataSet.Values.Select(c => c.Data).ToList();
    }
}
