﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numpty.DataBase.Data.Core
{
    public static class DBUtils
    {
        private static int _id = 0;

        public static int NextGUID()
        {
            return (++_id);
        }
    }
}
