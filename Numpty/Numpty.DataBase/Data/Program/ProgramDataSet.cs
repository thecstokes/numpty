﻿using Numpty.DataBase.Data.Core;
using Numpty.DataBase.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numpty.DataBase.Data
{
    public class ProgramDataSet : BaseDataSet<ProgramDataSet, Program>
    {
        public static readonly ProgramDataSet SoftwareEngineering = new ProgramDataSet(new Program
        {
            ID = DBUtils.NextGUID(),
            Name = "Software Engineering",
            Years = new List<ProgramYear>
            {
                new ProgramYear
                {
                    ID = DBUtils.NextGUID(),
                    Year = 4,
                    Courses = new List<Course>
                    {
                        CourseDataSet._3Y03.Data,
                        CourseDataSet._4AA4.Data,
                        CourseDataSet._4A03.Data,
                        CourseDataSet._4HC3.Data,
                        CourseDataSet._4TA6.Data
                    }
                }
            }
        });

        public ProgramDataSet(Program program) : base(program)
        {

        }

        protected override ProgramDataSet Instance => this;
    }
}
