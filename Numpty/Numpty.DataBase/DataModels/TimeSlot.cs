﻿using Numpty.DataBase.DataModels.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numpty.DataBase.DataModels
{
    public class TimeSlot : IDataModel
    {
        public int ID { get; set; }

        public TimeSlotType Type { get; set; }

        public WeekDay Day { get; set; }

        public Period Start { get; set; }

        public Period End { get; set; }
    }
}
