﻿using Numpty.DataBase.DataModels.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numpty.DataBase.DataModels
{
    public class ProgramYear : IDataModel
    {
        public int ID { get; set; }

        public int Year { get; set; }

        public List<Course> Courses { get; set; }
    }
}
