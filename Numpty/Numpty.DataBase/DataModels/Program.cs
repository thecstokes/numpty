﻿using Numpty.DataBase.DataModels.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numpty.DataBase.DataModels
{
    public class Program : IDataModel
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public List<ProgramYear> Years { get; set; }
    }
}
