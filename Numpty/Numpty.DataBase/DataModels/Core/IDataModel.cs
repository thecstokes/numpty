﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numpty.DataBase.DataModels.Core
{
    public interface IDataModel
    {
        int ID { get; set; }
    }
}
