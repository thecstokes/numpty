﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numpty.DataBase.DataModels
{
    public enum TimeSlotType
    {
        Core = 1,
        Lab = 2,
        Tutorial = 3
    }
}
