﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numpty.DataBase.DataModels
{
    /// <summary>
    /// Period is the time period of 60 minuets that a course can run in.
    /// </summary>
    public enum Period
    {
        /// <summary> 8:30 - 9:30 </summary>
        One = 1,
        /// <summary> 9:30 - 10:30 </summary>
        Two = 2,
        /// <summary> 10:30 - 11:30 </summary>
        Three = 3,
        /// <summary> 11:30 - 12:30 </summary>
        Four = 4,
        /// <summary> 12:30 - 13:30 </summary>
        Five = 5,
        /// <summary> 13:30 - 14:30 </summary>
        Six = 6,
        /// <summary> 14:30 - 15:30 </summary>
        Seven = 7,
        /// <summary> 15:30 - 16:30 </summary>
        Eight = 8,
        /// <summary> 16:30 - 17:30 </summary>
        Nine = 9,
        /// <summary> 17:30 - 18:30 </summary>
        Ten = 10,
        /// <summary> 18:30 - 19:30 </summary>
        Eleven = 11,
        /// <summary> 19:30 - 20:30 </summary>
        Twelve = 12,
        /// <summary> 20:30 - 21:30 </summary>
        Thirteen = 13,
        /// <summary> 21:30 - 22:30 </summary>
        Fourteen = 14,
        /// <summary> 22:30 - 23:30 </summary>
        Fifteen = 15,
        /// <summary> 23:30 - 24:30 </summary>
        Sixteen = 16
    }
}
